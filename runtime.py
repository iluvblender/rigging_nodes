'''
Module for storing dictionaries and values needed for caching and executing the node trees
'''
import time

# To avoid long lookups to know which socket is connected to which
cache_socket_conections = dict()
# To avoid searching for the group outputs on each execution
cache_node_group_outputs = dict()
# cache get/set nodes
cache_tree_portals = dict()
# socket values are stored here
cache_socket_variables = dict()
# caches the return value of "get_dependants" function
cache_node_dependants = dict()

# to debug the execution times
cache_node_times = dict()
cache_nodetree_times = dict()

# Loop related information
cache_loop_index = {}
cache_loop_inputs = {}

runtime_info = {
    'updating': False,
    'executing': False,
}


class MeasureTime():
    '''Class to use with the "with" statement that logs the times of execution for the given node'''

    def __init__(self, node, name):
        self.node = node
        self.name = name

    def __enter__(self):
        self.start_time = time.perf_counter_ns()

    def __exit__(self, type, value, traceback):
        end_time = time.perf_counter_ns()
        total_time = (float((end_time - self.start_time))) / 1000000000

        node_dict = cache_node_times.setdefault(self.node.id_data, {}).setdefault(self.node, {})

        if self.name in {'Execution', 'Gather Inputs', 'Focus', 'Set Outputs', 'Mode Change', 'Get Dependants'}:
            if self.name in cache_nodetree_times:
                cache_nodetree_times[self.name] += total_time
            else:
                cache_nodetree_times[self.name] = total_time

        if self.name in node_dict:
            node_dict[self.name] += total_time
        else:
            node_dict[self.name] = total_time


class MeasureTreeTime():
    '''Class to use with the "with" statement that logs the times of execution for the given node'''

    def __init__(self, name):
        self.name = name

    def __enter__(self):
        self.start_time = time.perf_counter_ns()

    def __exit__(self, type, value, traceback):
        end_time = time.perf_counter_ns()
        total_time = (float((end_time - self.start_time))) / 1000000000

        if self.name in cache_nodetree_times:
            cache_nodetree_times[self.name] += total_time
        else:
            cache_nodetree_times[self.name] = total_time
