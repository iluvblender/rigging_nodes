from bpy.types import Operator
import math
import bpy
from collections import OrderedDict

import warnings
import bpy
from ..preferences import preferences


class BBN_OP_generate_rig(Operator):
    '''Execute the node tree up to the previewed node'''
    bl_idname = "bbn.generate_rig"
    bl_label = "Execute"
    bl_options = {"REGISTER", "UNDO"}

    node_name: bpy.props.StringProperty()

    @classmethod
    def poll(cls, context):
        return context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree'

    def execute(self, context):
        bone_tree = context.space_data.edit_tree
        if self.node_name:
            bone_tree.set_preview(self.node_name)
        with warnings.catch_warnings(record=True) as w:
            warnings.simplefilter(preferences.warning_behaviour)
            bone_tree.execute(context)

            if w:
                self.report({'INFO'}, 'To track the warnings, set the warning behaviour dropdown to "Error" in the "Rigging Nodes" addon settings. Set it to "Ignore" to avoid these messages instead.')
            for warning in w:
                self.report({'WARNING'}, str(warning.message))
            return {'FINISHED'}
