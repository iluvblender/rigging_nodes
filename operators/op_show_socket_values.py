from bpy.types import Operator
import bpy


class BBN_OP_show_shocket_value(Operator):
    bl_idname = "bbn.show_shocket_value"
    bl_label = ""

    value: bpy.props.StringProperty()

    bl_options = {'INTERNAL'}

    @ classmethod
    def description(self, context, properties):
        return properties.value

    def execute(self, context):
        return {'FINISHED'}

    def invoke(self, context, event):
        wm = context.window_manager
        return wm.invoke_props_dialog(self)

    def draw(self, context):
        strings = self.value.split('\n')
        layout = self.layout

        col = layout.column()
        for x in strings:
            col.label(text=x)
