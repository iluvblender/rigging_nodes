from bpy.types import Operator
import bpy


class BBN_OP_socket_description(Operator):
    bl_idname = "bbn.socket_description"
    bl_label = ""

    socket_name: bpy.props.StringProperty()
    value: bpy.props.StringProperty()
    node: bpy.props.StringProperty()

    bl_options = {'INTERNAL'}

    @ classmethod
    def description(self, context, properties):
        if properties.value:
            ans = properties.value
        else:
            ans = 'No description for this socket'

        return f'{properties.socket_name}\n{ans}'

    def execute(self, context):
        return {'PASS_THROUGH'}
