import bpy
import mathutils
import functools
from bpy.types import Node

from ..node_base import BBN_node


class BBN_node_subdivide_bone(Node, BBN_node):
    bl_idname = 'bbn_node_subdivide_bone'
    bl_label = "Subdivide Bone"
    bl_icon = 'BONE_DATA'

    bl_width_default = 250

    def warning(self, context):
        if not context.space_data.node_tree.auto_update:
            return''
        return '''This node tends to crash when
the auto-execute option is enabled in the
nodetree settings.
Disable it for a more stable experience'''

    bone_default: bpy.props.StringProperty(default='Bone')

    input_sockets = {
        'Armature': {'type': 'BBN_object_socket'},
    }

    output_sockets = {
        'Armature': {'type': 'BBN_object_socket'},
        'Bones': {'type': 'BBN_string_array_v2_socket'},
    }

    behaviour_enum = [
        ('SINGLE', 'Single', 'Subdivide one bone', '', 0),
        ('MULTIPLE', 'Multiple', 'Subdivide multiple bones', '', 1),
    ]

    behaviour_sockets = {
        'SINGLE': {
            'INPUTS': {
                'Bone': {'type': 'BBN_bone_socket'},
                'Subd': {'type': 'BBN_int_socket', 'default_value': 2},
            }
        },
        'MULTIPLE': {
            'INPUTS': {
                'Bone': {'type': 'BBN_string_array_v2_socket'},
                'Subd': {'type': 'BBN_int_socket', 'default_value': 2},
            }
        },
    }

    _opt_input_sockets = {
        'New Name': {'type': 'BBN_string_socket'},
    }

    input_to_focus = 'Armature'
    focus_mode = {'EDIT'}

    def subdivide_bone(self, context, armature_obj, bone_name, subd, new_name, path):
        ans = []

        if subd <= 0:
            return [bone_name]
        bone = armature_obj.data.edit_bones[bone_name]

        override = context.copy()
        override['selected_objects'] = override['selected_editable_objects'] = [armature_obj]
        override['object'] = override['active_object'] = armature_obj
        override['active_edit_bone'] = bone
        override['selected_editable_bones'] = [bone]

        old_bones = {x.name for x in armature_obj.data.edit_bones}

        bpy.ops.armature.subdivide(override, number_cuts=subd)

        del override

        new_bones = [bone] + [x for x in armature_obj.data.edit_bones if x.name not in old_bones]

        bone_parents = []
        for intersect_bone in new_bones:
            parents = []
            for i in range(subd + 1):
                if intersect_bone.parent:
                    parents.append(intersect_bone.parent)
                    intersect_bone = intersect_bone.parent
                else:
                    break

            bone_parents.append(parents)

        # blender names the dubdivided bones in a weird way, this renames them correctly
        def greater(a, b):
            if (b[0] in a[1]):
                return 1
            return -1

        sorted_bones = [x[0] for x in sorted(((new_bones[i], bone_parents[i]) for i in range(len(new_bones))), key=functools.cmp_to_key(greater))]

        if new_name:
            for x in sorted_bones:
                x.name = new_name
                armature_obj.data.BBN_info.add_bone_tracking(x.name, self, path)
        else:
            # TODO: maybe in a future update they fix the order in which bones are named using the subdivide operator, and this should be skipped
            old_names = [x.name for x in sorted_bones]
            old_names.append(old_names.pop(0))
            old_names.reverse()
            for x in sorted_bones:
                x.name = '__temp__'
            for i, x in enumerate(sorted_bones):
                x.name = old_names[i]
                armature_obj.data.BBN_info.add_bone_tracking(x.name, self, path)

        return [x.name for x in sorted_bones]

    def process(self, context, id, path):
        armature = self.get_input_value("Armature", required=True)
        bone_names = self.get_input_value('Bone')
        subd = self.get_input_value('Subd')
        new_name = self.get_input_value('New Name', default='')

        ans = []
        if self.current_behaviour == 'SINGLE':
            bone_names = [bone_names]

        for bone_name in bone_names:
            ans.extend(self.subdivide_bone(context, armature, bone_name, subd, new_name, path))

        self.set_output_value('Bones', ans)
        self.set_output_value('Armature', armature)
