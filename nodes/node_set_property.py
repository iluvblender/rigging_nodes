import bpy
from .node_base import BBN_node
from mathutils import Vector


props_dict = {}

extra_properties = {
}

extra_sockets = {
    bpy.types.EditBone: {
        'Roll (Z Axis)': {'type': 'BBN_vector_socket', 'default_value': [0, 0, 1], 'override_name': 'roll', 'display_name': 'Roll (Z Axis)'},
    },
}


class BBN_node_set_property(BBN_node):
    bl_idname = 'bbn_set_property_node'
    bl_label = "Set Bone Property"
    bl_icon = 'BONE_DATA'

    bl_width_default = 300

    rna_props_to_get = {'name', 'description', 'type', 'subtype', 'enum_items', 'default', 'fixed_type', 'icon', 'is_array', 'array_dimensions', 'array_length', 'default_array'}
    props_to_get = {}
    props_to_ignore = {}

    def draw_label(self):
        if len(self.inputs) == len(self.input_sockets.keys()) + 1:
            return f'Set {self.inputs[(len(self.inputs) - 1)].name}'
        return self.bl_label

    @property
    def props_class(self):
        return bpy.types.PoseBone

    def get_props(self, cls):
        '''
        Inspects the given class and returns a dictionary with all the necessary property data to create input sockets.
        To avoid too many lookups and checks to this data, it is only calculated once for each class and it's cached in the props_dict global variable
        '''
        if not cls:
            return {}
        if cls in props_dict:
            return props_dict[cls]

        if self.props_to_get:
            props = (x for x in cls.bl_rna.properties if x.identifier in self.props_to_get)
        else:
            props = cls.bl_rna.properties

        props = (x for x in props if x.identifier not in self.props_to_ignore)

        ans = {x.identifier: {y.identifier: getattr(x, y.identifier) for y in x.bl_rna.properties if y.identifier in self.rna_props_to_get} for x in props if not x.is_readonly}

        if 'space_subtarget' in ans:
            ans['space_subtarget']['name'] = 'Sub-Target Space'

        ans.update(extra_properties)

        props_dict[cls] = ans
        return ans

    def get_current_props(self):
        return self.get_props(self.props_class)

    @ property
    def opt_input_sockets(self):
        props = self.get_current_props()
        ans = {}
        if self._current_behaviour != 'MULTIPLE_VALUES':
            for prop, prop_info in props.items():
                if prop_info['type'] == 'STRING':
                    ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_string_socket', 'default_value': prop_info['default']}
                elif prop_info['type'] == 'ENUM':
                    ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_enum_socket', 'default_value': prop_info['default'], 'items': [(x.identifier, x.name, x.description, x.icon, i) for i, x in enumerate(prop_info['enum_items'])]}
                elif prop_info['type'] == 'BOOLEAN':
                    if prop_info['is_array'] is False:
                        ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_bool_socket', 'default_value': prop_info['default']}
                    elif prop_info['array_length'] < 5:
                        ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_bool_array_socket', 'required_length': prop_info['array_length']}
                    else:
                        ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_int_socket'}
                elif prop_info['type'] == 'POINTER':
                    if prop_info['fixed_type'].bl_rna.identifier == 'Object':
                        ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_object_ref_socket'}
                    elif prop_info['fixed_type'].bl_rna.identifier == 'EditBone':
                        ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_bone_socket'}
                    elif prop_info['fixed_type'].bl_rna.identifier == 'Bone':
                        ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_bone_socket'}
                    elif prop_info['fixed_type'].bl_rna.identifier == 'PoseBone':
                        ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_bone_socket'}
                    elif prop_info['fixed_type'].bl_rna.identifier == 'Object':
                        ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_object_ref_socket'}
                    elif prop_info['fixed_type'].bl_rna.identifier == 'Action':
                        ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_action_socket'}
                    elif prop_info['fixed_type'].bl_rna.identifier == 'Text':
                        ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_text_socket'}
                elif prop_info['type'] == 'FLOAT':
                    if prop_info['is_array'] is False:
                        ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_float_socket', 'default_value': prop_info['default']}
                    else:
                        if prop_info['array_length'] == 3:
                            ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_vector_socket', 'default_value': prop_info['default_array']}
                        elif prop_info['subtype'] == 'MATRIX':
                            if prop_info['array_length'] == 16:
                                ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_matrix_socket', 'default_value': prop_info['default_array']}
                        else:
                            ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_float_array_socket', 'required_length': prop_info['array_length'], 'default_value': prop_info['default_array']}
                elif prop_info['type'] == 'INT':
                    ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_int_socket', 'default_value': prop_info['default']}
                else:
                    pass
                    # print(prop)
        else:
            for prop, prop_info in props.items():
                if prop_info['type'] == 'STRING':
                    ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_string_array_v2_socket'}
                elif prop_info['type'] == 'ENUM':
                    ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_string_array_v2_socket'}
                elif prop_info['type'] == 'BOOLEAN':
                    if prop_info['is_array'] is False:
                        ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_bool_array_socket'}
                    elif prop == 'layers':
                        ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_int_array_socket'}
                elif prop_info['type'] == 'POINTER':
                    if prop_info['fixed_type'].bl_rna.identifier == 'Object':
                        ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_object_ref_array_socket'}
                    elif prop_info['fixed_type'].bl_rna.identifier == 'EditBone':
                        ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_string_array_v2_socket'}
                    elif prop_info['fixed_type'].bl_rna.identifier == 'Bone':
                        ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_string_array_v2_socket'}
                    elif prop_info['fixed_type'].bl_rna.identifier == 'PoseBone':
                        ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_string_array_v2_socket'}
                    elif prop_info['fixed_type'].bl_rna.identifier == 'Object':
                        ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_object_ref_array_socket'}
                    elif prop_info['fixed_type'].bl_rna.identifier == 'Action':
                        ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_action_array_socket'}
                    elif prop_info['fixed_type'].bl_rna.identifier == 'Text':
                        ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_text_array_socket'}
                elif prop_info['type'] == 'FLOAT':
                    if prop_info['is_array'] is False:
                        ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_float_array_socket'}
                    else:
                        if prop_info['array_length'] == 3:
                            ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_vector_array_socket'}
                        elif prop_info['subtype'] == 'MATRIX' and prop_info['array_length'] == 16:
                            ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_matrix_array_socket'}
                elif prop_info['type'] == 'INT':
                    ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_int_array_socket'}
                else:
                    pass
                    # print(prop)
        for key in ans.keys():
            ans[key]['is_deletable'] = True

        ans.update(extra_sockets.get(self.props_class, {}))

        return ans

    def get_socket_data(self, id_obj, socket, prop_name):
        '''
        Gets the value of a socket and converts it to the actual data type it represents.
        In the case of pointing to a bone, it will convert the input string to the bone type
        In the case of long array of bools, it converts an integer to an array of bools with the given index as the only True value
        It can handle Arrays sockets by checking the current behaviour.
        '''
        val = socket.get_real_value()

        prop_info = self.get_current_props()[prop_name]

        if prop_info['type'] == 'BOOLEAN' and prop_info['array_length'] >= 5:
            if self._current_behaviour == 'MULTIPLE_VALUES':
                val = [tuple(i == x for i in range(prop_info['array_length'])) for x in val]
            else:
                val = tuple(i == val for i in range(prop_info['array_length']))

        elif prop_info['type'] == 'POINTER':
            val = socket.get_real_value()
            if self._current_behaviour != 'MULTIPLE_VALUES':
                if val:
                    if prop_info['fixed_type'].bl_rna.identifier == 'EditBone':
                        return id_obj.data.edit_bones[val]
                    elif prop_info['fixed_type'].bl_rna.identifier == 'Bone':
                        return id_obj.data.bones[val]
                    elif prop_info['fixed_type'].bl_rna.identifier == 'PoseBone':
                        return id_obj.pose.bones[val]
                    elif prop_info['fixed_type'].bl_rna.identifier == 'Object':
                        return val
                    else:
                        return val
                else:
                    if prop_info['fixed_type'].bl_rna.identifier == 'EditBone':
                        return None
                    elif prop_info['fixed_type'].bl_rna.identifier == 'Bone':
                        return None
                    elif prop_info['fixed_type'].bl_rna.identifier == 'PoseBone':
                        return None
                    elif prop_info['fixed_type'].bl_rna.identifier == 'Object':
                        return None
                    else:
                        return val
            else:
                if val:
                    if prop_info['fixed_type'].bl_rna.identifier == 'EditBone':
                        return [id_obj.data.edit_bones[x] if x else None for x in val]
                    elif prop_info['fixed_type'].bl_rna.identifier == 'Bone':
                        return [id_obj.data.bones[x] if x else None for x in val]
                    elif prop_info['fixed_type'].bl_rna.identifier == 'PoseBone':
                        return [id_obj.pose.bones[x] if x else None for x in val]
                    elif prop_info['fixed_type'].bl_rna.identifier == 'Object':
                        return val
                    else:
                        return val

        return val

    def set_attribute(self, obj, attrib_name, value, socket):
        '''
        Sets the atribute of the data to the given value, this can be overriden to check for special cases
        For now, only a special case of the "roll" socket is overriden
        '''
        if socket.name == 'roll' and socket.bl_rna.name == 'BBN_vector_socket':
            value.normalize()
            obj.align_roll(value)
        else:
            setattr(obj, attrib_name, value)

    def set_prop(self, id_obj, socket, data, prop_name, index=0):
        val = self.get_socket_data(id_obj, socket, prop_name)

        if self._current_behaviour == 'SINGLE' or self._current_behaviour == 'MULTIPLE_SUBTARGETS' or not hasattr(data, '__iter__'):
            self.set_attribute(data, prop_name, val, socket)
        elif self._current_behaviour == 'MULTIPLE':
            for x in data:
                self.set_attribute(x, prop_name, val, socket)
        elif self._current_behaviour == 'MULTIPLE_VALUES':
            if len(val) != len(data):
                raise ValueError(f"Array lengths of {prop_name} don't match")
            for y, x in zip(val, data):
                self.set_attribute(x, prop_name, y, socket)

    def set_props(self, id_obj, data, start_index=0):
        '''
        Function that goes through the socket inputs and assigns the correct values to the given data,

            id_obj: is a reference to a bpy.types.Object object used for converting strings to bones
            data: the actual object the socket data will be applied to
            start_index: force the function to ignore the first X inputs of the node
        '''
        for socket in self.inputs[start_index:]:
            if socket.name in self.opt_input_sockets.keys():
                self.set_prop(id_obj, socket, data, socket.name)

            else:
                pass
                #print(f'{self} {socket.name} was not set')
