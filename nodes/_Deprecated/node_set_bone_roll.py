import bpy
import mathutils
from bpy.types import Node

from ..node_base import BBN_node
import warnings


class BBN_node_set_bone_roll(Node, BBN_node):
    deprecated = True

    bl_idname = 'bbn_node_set_bone_roll'
    bl_label = "Set Bone Roll (LEGACY)"
    bl_icon = 'BONE_DATA'

    bone_default: bpy.props.StringProperty(default='Bone')

    def warning(self, context):
        return '''It's recomended to replace this node with the "Set Bone Property" node\nuse the "Roll (Z Axis)" socket'''

    node_version = 2

    input_sockets = {
        'Armature': {'type': 'BBN_object_socket'},
    }

    output_sockets = {
        'Armature': {'type': 'BBN_object_socket'},
    }

    behaviour_enum = [
        ('SINGLE', 'Single', 'Aligns the z axis of the bone to the given vector', '', 0),
        ('MULTIPLE', 'Multiple', 'Aligns the z axis of the bone to the given vector', '', 1),
    ]

    behaviour_sockets = {
        'SINGLE': {
            'INPUTS': {
                'Bone': {'type': 'BBN_bone_socket'},
                'Vector': {'type': 'BBN_vector_socket'},
            },
            'OUTPUTS': {
                'Bone': {'type': 'BBN_string_socket'},
            }
        },
        'MULTIPLE': {
            'INPUTS': {
                'Bone': {'type': 'BBN_string_array_v2_socket'},
                'Vector': {'type': 'BBN_vector_socket'},
            },
            'OUTPUTS': {
                'Bone': {'type': 'BBN_string_array_v2_socket'},
            }
        },
    }

    input_to_focus = 'Armature'
    focus_mode = {'EDIT'}

    def upgrade_node(self):
        super().upgrade_node()

        # remap old properties
        if 'execute_mode' in self.keys():
            self['current_behaviour'] = self['execute_mode']
            del self['execute_mode']

        self.current_node_version = self.node_version

    def process(self, context, id, path):
        armature = self.get_input_value("Armature", required=True)
        bone_names = self.get_input_value('Bone')
        vector = self.get_input_value('Vector')
        vector.normalize()  # align_roll sometimes fails if the vector is not normalized

        if self.current_behaviour == 'SINGLE':
            bone_names = [bone_names]

        for bone_name in bone_names:
            bone = armature.data.edit_bones[bone_name]
            bone.align_roll(vector)
            armature.data.BBN_info.add_bone_tracking(bone_name, self, path)

        if self.current_behaviour == 'SINGLE':
            self.outputs['Bone'].set_value(bone_names[0])
        else:
            self.outputs['Bone'].set_value(bone_names)
        self.outputs['Armature'].set_value(armature)

        warnings.warn('Set bone roll is deprecated\n use "Set Bone Property" node instead\nuse the "Roll (Z Axis)" socket', DeprecationWarning)
