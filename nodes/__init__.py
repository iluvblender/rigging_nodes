import nodeitems_utils
from nodeitems_utils import NodeCategory, NodeItem
import os
import importlib
from bpy.types import Key, Node, NodeCustomGroup, NodeGroupInput, NodeReroute
import inspect
import bpy


nodes_dict_classes = {}
nodes_dict = {}

node_classes = set()

categories = [x for x in os.listdir(os.path.dirname(__file__)) if os.path.isdir(os.path.join(os.path.dirname(__file__), x))]
for category in categories:

    files = [x[:-3] for x in os.listdir(os.path.join(os.path.dirname(__file__), category)) if x.endswith('.py') and not x.startswith('_')]

    if not files:
        continue

    nodes_dict_classes[category] = list()

    for i in files:
        importlib.import_module('.' + category + '.' + i, package=__package__)

    __globals = globals().copy()

    module = __globals[category]

    for x in [x for x in dir(module) if x.startswith('node_')]:
        sub_module = getattr(module, x)
        for y in [item for item in dir(sub_module)]:
            if not y.startswith('BBN_node_'):
                continue
            node = getattr(sub_module, y)
            if node != NodeReroute and node != Node and node != NodeCustomGroup and node != NodeGroupInput and inspect.isclass(node) and issubclass(node, Node) and node not in node_classes:
                # globals()[y] = node
                node_classes.add(node)
                if not node.deprecated:
                    nodes_dict_classes[category].append(node)

# SETUP CATEGORIES
nodes_dict_classes['Group'].extend([bpy.types.NodeGroupInput.bl_rna, bpy.types.NodeGroupOutput.bl_rna])
nodes_dict_classes['Loop'].extend([bpy.types.NodeGroupInput.bl_rna, bpy.types.NodeGroupOutput.bl_rna])
nodes_dict_classes['Flow_Control'].extend([bpy.types.NodeFrame.bl_rna])

for category, classes in sorted(nodes_dict_classes.items()):
    if classes:
        nodes_dict[category] = [x.bl_idname if hasattr(x, 'bl_idname') else x.identifier for x in sorted(classes, key=lambda a:a.bl_label if hasattr(a, 'bl_label') else a.name)]


class BBN_node_category(NodeCategory):
    @ classmethod
    def poll(cls, context):
        return context.space_data.tree_type == 'bbn_tree'


node_categories = [BBN_node_category(category, category, items=[NodeItem(node) for node in node_classes]) for category, node_classes in sorted(nodes_dict.items(), reverse=False)]


def register():
    from bpy.utils import register_class

    for cls in node_classes:
        cls.register_dependants()

    for cls in node_classes:
        #print(f'registering {cls.bl_idname} ...')
        #print(f'bases: {cls.__bases__} ...')
        register_class(cls)

    nodeitems_utils.register_node_categories('Rigging Nodes', node_categories)


def unregister():
    nodeitems_utils.unregister_node_categories('Rigging Nodes')

    from bpy.utils import unregister_class

    for cls in node_classes:
        #print(f'unregistering {cls.bl_idname} ...')
        unregister_class(cls)

    for cls in node_classes:
        cls.unregister_dependants()
