import bpy
import mathutils
from bpy.types import Node

from ..node_base import BBN_node
from ...node_tree import BBN_tree


class BBN_node_edit_bone_groups(Node, BBN_node):
    bl_idname = 'bbn_node_edit_bone_groups'
    bl_label = "Handle Bone Groups"
    bl_icon = 'OUTLINER_OB_ARMATURE'

    bl_width_default = 300

    output_sockets = {
        'Armature': {'type': 'BBN_object_socket'},
        'Index': {'type': 'BBN_int_socket'},
    }

    input_sockets = {
        'Armature': {'type': 'BBN_object_socket'},
        'Name': {'type': 'BBN_string_socket'},
        'Color Set': {'type': 'BBN_enum_socket',
                      'items': [(x.identifier, x.name, x.description, x.icon, i) for i, x in enumerate(bpy.types.BoneGroup.bl_rna.properties['color_set'].enum_items)]
                      },
    }

    _opt_input_sockets = {
        'active color': {'type': 'BBN_vector_socket'},
        'normal color': {'type': 'BBN_vector_socket'},
        'select color': {'type': 'BBN_vector_socket'},
        'Bone': {'type': 'BBN_bone_socket', 'override_name': 'bone'},
        'Bones': {'type': 'BBN_string_array_v2_socket', 'override_name': 'bone'},
    }

    input_to_focus = 'Armature'
    focus_mode = {'POSE'}

    def process(self, context, id, path):
        armature = self.get_input_value("Armature")
        bgs = armature.pose.bone_groups
        group_name = self.get_input_value("Name")

        if group_name not in bgs:
            bg = bgs.new(name=group_name)
            bg.color_set = self.get_input_value("Color Set")
            active = self.get_input_value("active color")
            normal = self.get_input_value("normal color")
            select = self.get_input_value("select color")
            if active:
                bg.colors.active = active
            if normal:
                bg.colors.normal = normal
            if select:
                bg.colors.select = select

        index = next(i for i, x in enumerate(bgs) if x.name == group_name)
        self.set_output_value('Index', index)

        bones = self.get_input_value('bone', default=[])
        if bones and type(bones) is str:
            bones = [bones]

        for bone in bones:
            pose_bone = armature.pose.bones[bone]
            pose_bone.bone_group_index = index
            armature.data.BBN_info.add_bone_tracking(bone, self, path)

        self.set_output_value(0, armature)
