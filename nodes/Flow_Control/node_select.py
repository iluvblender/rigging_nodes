from nodeitems_utils import NodeCategory, NodeItem
import nodeitems_utils
import bpy
import mathutils
from bpy.types import Node

from ..node_base import BBN_node
from ...runtime import runtime_info


class BBN_UL_enum_editor(bpy.types.UIList):
    def draw_item(self, context, layout, data, item, icon, active_data, active_propname, index):
        row = layout.row()
        row.prop(item, 'value', text='', emboss=True)
        op = row.operator('bbn.remove_enum_node', text='', icon='CANCEL')
        op.delete_enum = item.value
        op.node = data.node.name


class BBN_OP_remove_enum_node(bpy.types.Operator):
    bl_idname = "bbn.remove_enum_node"
    bl_label = "Remove Enum"
    bl_icon = 'CANCEL'

    bl_options = {'REGISTER', 'UNDO', 'INTERNAL'}

    node: bpy.props.StringProperty()
    delete_enum: bpy.props.StringProperty()

    @classmethod
    def poll(cls, context):
        return context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree'

    def execute(self, context):
        tree = context.space_data.edit_tree

        node = tree.nodes.get(self.node)
        node.inputs[0].enum_items.remove(next(i for i, x in enumerate(node.inputs[0].enum_items) if x.value == self.delete_enum))

        return {'FINISHED'}


class BBN_OP_add_enum_node(bpy.types.Operator):
    bl_idname = "bbn.add_enum_node"
    bl_label = "Add Enum"
    bl_icon = 'ADD'

    bl_options = {'REGISTER', 'UNDO', 'INTERNAL'}

    node: bpy.props.StringProperty()

    @classmethod
    def poll(cls, context):
        return context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree'

    def execute(self, context):
        tree = context.space_data.edit_tree

        node = tree.nodes.get(self.node)

        a = node.inputs[0].enum_items.add()
        a.value = 'Test'
        return {'FINISHED'}


class BBN_OP_edit_enum_node(bpy.types.Operator):
    bl_idname = "bbn.edit_enum_node"
    bl_label = "Edit Enum"
    bl_icon = 'ADD'

    bl_options = {'REGISTER', 'UNDO', 'INTERNAL'}

    node: bpy.props.StringProperty()
    selected_index: bpy.props.IntProperty()

    @classmethod
    def poll(cls, context):
        return context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree'

    def draw(self, context):
        tree = context.space_data.edit_tree

        node = tree.nodes.get(self.node)

        self.layout.operator('bbn.add_enum_node').node = node.name

        self.layout.template_list("BBN_UL_enum_editor", "", node.inputs[0], "enum_items", self, "selected_index", rows=4)

    def invoke(self, context, event):
        wm = context.window_manager
        return wm.invoke_props_dialog(self)

    def execute(self, context):
        tree = context.space_data.edit_tree

        node = tree.nodes.get(self.node)

        node.create_enum_sockets()

        return {'FINISHED'}


class BBN_node_select(Node, BBN_node):
    bl_idname = 'bbn_select_node'
    bl_label = "Select"
    bl_icon = 'RIGHTARROW_THIN'
    bl_width_default = 200

    input_sockets = {
        'Enum': {'type': 'BBN_enum_socket'},
        '...': {'type': 'BBN_add_array_socket'},
    }

    socket_type: bpy.props.StringProperty()

    dependent_classes = [BBN_UL_enum_editor, BBN_OP_remove_enum_node, BBN_OP_edit_enum_node, BBN_OP_add_enum_node]

    def draw_buttons(self, context, layout):
        layout, row1, row2 = self.setup_buttons(context, layout)

        row1.operator('bbn.edit_enum_node').node = self.name

    def create_enum_sockets(self):
        if self.socket_type:
            for enum in self.inputs[0].enum_items:
                if enum.value in self.inputs and self.socket_type == self.inputs.get(enum.value).bl_rna.bl_idname:
                    continue
                self.change_socket(self.inputs, enum.value, {'type': self.socket_type, 'display_name': enum.label})

            for i in reversed(range(1, len(self.inputs))):
                x = self.inputs[i]
                if x.name not in [x.value for x in self.inputs[0].enum_items]:
                    self.inputs.remove(x)

    def execute_dependants(self, context, id, path):
        connected_socket = self.inputs[0].connected_socket
        if connected_socket:
            x = connected_socket.node
            self.execute_other(context, id, path, x)

        for x in self.inputs[1:]:
            if x.name == self.get_input_value(0):
                connected_socket = x.connected_socket
                if connected_socket:
                    x = connected_socket.node
                    self.execute_other(context, id, path, x)
                break

    def process(self, context, id, path):
        for x in self.inputs[1:]:
            if x.name == self.get_input_value(0):
                self.outputs[0].set_value(self.get_input_value(x.name))
                break

    def update(self):
        if hasattr(self, 'initializing') and self.initializing:
            return
        if not self.can_update():
            return
        self.update_others()

        main_input = self.inputs[1]
        connected_socket = main_input.connected_socket

        if main_input.bl_idname == 'BBN_add_array_socket' and connected_socket:
            self.socket_type = connected_socket.bl_rna.name
            self.inputs.remove(main_input)

            self.create_enum_sockets()

            new_socket = self.outputs.new(self.socket_type, 'Out')._init()
            new_socket.init_from_socket(connected_socket.node, connected_socket)

        self.remove_incorrect_links()
