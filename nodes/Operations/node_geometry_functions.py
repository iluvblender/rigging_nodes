import bpy
from bpy.types import Node

from ..node_base import BBN_node

import mathutils

functions = [
    mathutils.geometry.normal,
    mathutils.geometry.area_tri,
    mathutils.geometry.barycentric_transform,
    mathutils.geometry.distance_point_to_plane,
    mathutils.geometry.interpolate_bezier,
    mathutils.geometry.intersect_line_line,
    # mathutils.geometry.intersect_line_line_2d,
    mathutils.geometry.intersect_line_plane,
    mathutils.geometry.intersect_line_sphere,
    # mathutils.geometry.intersect_line_sphere_2d,
    mathutils.geometry.intersect_plane_plane,
    mathutils.geometry.intersect_point_line,
    # mathutils.geometry.intersect_point_quad_2d,
    # mathutils.geometry.intersect_point_tri_2d,
    mathutils.geometry.intersect_ray_tri,
]

functions_map = {x.__name__: x for x in functions}


functions_enum = sorted([(x, x.replace('_', ' ').title(), x.replace('_', ' ').title(), 'NONE', i) for i, x in enumerate(functions_map.keys())], key=lambda a: a[1])


class BBN_node_geometry_functions(Node, BBN_node):
    bl_idname = 'bbn_node_geometry_functions'
    bl_label = "Geometry Functions"
    bl_icon = 'PANEL_CLOSE'
    bl_width_default = 300

    behaviour_enum = functions_enum

    behaviour_sockets = {
        'normal': {
            'INPUTS': {
                'Point 1': {'type': 'BBN_vector_socket'},
                'Point 2': {'type': 'BBN_vector_socket'},
                'Point 3': {'type': 'BBN_vector_socket'},
            },
            'OUTPUTS': {
                'Normal': {'type': 'BBN_vector_socket'},
            }
        },
        'area_tri': {
            'INPUTS': {
                'Point 1': {'type': 'BBN_vector_socket'},
                'Point 2': {'type': 'BBN_vector_socket'},
                'Point 3': {'type': 'BBN_vector_socket'},
            },
            'OUTPUTS': {
                'Normal': {'type': 'BBN_float_socket'},
            }
        },
        'barycentric_transform': {
            'INPUTS': {
                'Point': {'type': 'BBN_vector_socket'},
                'Source Point 1': {'type': 'BBN_vector_socket'},
                'Source Point 2': {'type': 'BBN_vector_socket'},
                'Source Point 3': {'type': 'BBN_vector_socket'},
                'Target Point 1': {'type': 'BBN_vector_socket'},
                'Target Point 2': {'type': 'BBN_vector_socket'},
                'Target Point 3': {'type': 'BBN_vector_socket'},
            },
            'OUTPUTS': {
                'Transformed Point': {'type': 'BBN_vector_socket'},
            }
        },
        'distance_point_to_plane': {
            'INPUTS': {
                'Point': {'type': 'BBN_vector_socket'},
                'Plane Coord': {'type': 'BBN_vector_socket'},
                'Plane Normal': {'type': 'BBN_vector_socket'},
            },
            'OUTPUTS': {
                'Distance': {'type': 'BBN_float_socket'},
            }
        },
        'interpolate_bezier': {
            'INPUTS': {
                'Knot 1': {'type': 'BBN_vector_socket'},
                'Handle 1': {'type': 'BBN_vector_socket'},
                'Handle 2': {'type': 'BBN_vector_socket'},
                'Knot 2': {'type': 'BBN_vector_socket'},
                'Resolution ': {'type': 'BBN_int_socket', 'default_value': 2},
            },
            'OUTPUTS': {
                'Positions': {'type': 'BBN_vector_array_socket'},
            }
        },
        'intersect_line_line': {
            'INPUTS': {
                'Line 1.1': {'type': 'BBN_vector_socket'},
                'Line 1.2': {'type': 'BBN_vector_socket'},
                'Line 2.1': {'type': 'BBN_vector_socket'},
                'Line 2.2': {'type': 'BBN_vector_socket'},
            },
            'OUTPUTS': {
                'Closest to 1': {'type': 'BBN_vector_socket'},
                'Closest to 2': {'type': 'BBN_vector_socket'},
            }
        },
        'intersect_line_line_2d': {
            'INPUTS': {
                'Line 1.1': {'type': 'BBN_vector_socket'},
                'Line 1.2': {'type': 'BBN_vector_socket'},
                'Line 2.1': {'type': 'BBN_vector_socket'},
                'Line 2.2': {'type': 'BBN_vector_socket'},
            },
            'OUTPUTS': {
                'Intersection Point': {'type': 'BBN_vector_socket'},
            }
        },
        'intersect_line_plane': {
            'INPUTS': {
                'Line 1.1': {'type': 'BBN_vector_socket'},
                'Line 1.2': {'type': 'BBN_vector_socket'},
                'Plane Coord': {'type': 'BBN_vector_socket'},
                'Plane Normal': {'type': 'BBN_vector_socket'},
                'No Flip': {'type': 'BBN_bool_socket'},
            },
            'OUTPUTS': {
                'Intersection Point': {'type': 'BBN_vector_socket'},
            }
        },
        'intersect_line_sphere': {
            'INPUTS': {
                'Line 1.1': {'type': 'BBN_vector_socket'},
                'Line 1.2': {'type': 'BBN_vector_socket'},
                'Sphere Coord': {'type': 'BBN_vector_socket'},
                'Sphere Radius': {'type': 'BBN_float_socket'},
            },
            'OUTPUTS': {
                'Intersection Point 1': {'type': 'BBN_vector_socket'},
                'Intersection Point 2': {'type': 'BBN_vector_socket'},
            }
        },
        'intersect_line_sphere_2d': {
            'INPUTS': {
                'Line 1.1': {'type': 'BBN_vector_socket'},
                'Line 1.2': {'type': 'BBN_vector_socket'},
                'Sphere Coord': {'type': 'BBN_vector_socket'},
                'Sphere Radius': {'type': 'BBN_float_socket'},
            },
            'OUTPUTS': {
                'Intersection Point 1': {'type': 'BBN_vector_socket'},
                'Intersection Point 2': {'type': 'BBN_vector_socket'},
            }
        },
        'intersect_plane_plane': {
            'INPUTS': {
                'Plane a Coord': {'type': 'BBN_vector_socket'},
                'Plane a Normal': {'type': 'BBN_vector_socket'},
                'Plane b Coord': {'type': 'BBN_vector_socket'},
                'Plane b Normal': {'type': 'BBN_vector_socket'},
            },
            'OUTPUTS': {
                'Point': {'type': 'BBN_vector_socket'},
                'Vector': {'type': 'BBN_vector_socket'},
            }
        },
        'intersect_point_line': {
            'INPUTS': {
                'Point': {'type': 'BBN_vector_socket'},
                'Line 1.1': {'type': 'BBN_vector_socket'},
                'Line 1.2': {'type': 'BBN_vector_socket'},
            },
            'OUTPUTS': {
                'Point': {'type': 'BBN_vector_socket'},
                'Distance': {'type': 'BBN_float_socket'},
            }
        },
        'intersect_point_quad_2d': {
            'INPUTS': {
                'Point': {'type': 'BBN_vector_socket'},
                'Quad 1': {'type': 'BBN_vector_socket'},
                'Quad 2': {'type': 'BBN_vector_socket'},
                'Quad 3': {'type': 'BBN_vector_socket'},
                'Quad 4': {'type': 'BBN_vector_socket'},
            },
            'OUTPUTS': {
                'Is Inside': {'type': 'BBN_bool_socket'},
            }
        },
        'intersect_point_tri_2d': {
            'INPUTS': {
                'Point': {'type': 'BBN_vector_socket'},
                'Tri 1': {'type': 'BBN_vector_socket'},
                'Tri 2': {'type': 'BBN_vector_socket'},
                'Tri 3': {'type': 'BBN_vector_socket'},
            },
            'OUTPUTS': {
                'Is Inside': {'type': 'BBN_bool_socket'},
            }
        },
        'intersect_ray_tri': {
            'INPUTS': {
                'Tri 1': {'type': 'BBN_vector_socket'},
                'Tri 2': {'type': 'BBN_vector_socket'},
                'Tri 3': {'type': 'BBN_vector_socket'},
                'Ray': {'type': 'BBN_vector_socket'},
                'Orig': {'type': 'BBN_vector_socket'},
                'Clip': {'type': 'BBN_bool_socket'},
            },
            'OUTPUTS': {
                'Point': {'type': 'BBN_vector_socket'},
            }
        },
    }

    def draw_label(self):
        return self.current_behaviour.replace('_', ' ').title()

    def process(self, context, id, path):
        args = [self.get_input_value(i) for i in range(0, len(self.inputs))]

        func = functions_map[self.current_behaviour]

        result = func(*args)

        if (type(result) is list or type(result) is tuple) and len(self.outputs) > 1:
            for i, x in enumerate(result):
                self.outputs[i].set_value(x)
        else:
            if result is None:
                raise ValueError('The provided arguments do not meet the requirements')
            self.outputs[0].set_value(result)
