import bpy
from bpy.types import Node, Operator
from ..node_base import BBN_node

from ...node_tree import BBN_tree


class BBN_OP_setup_enum_items(Operator):
    bl_idname = "bbn.setup_enum_items"
    bl_label = "Setup Enum Items"

    bl_options = {'REGISTER', 'UNDO', 'INTERNAL'}

    node: bpy.props.StringProperty()

    @classmethod
    def poll(cls, context):
        return context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree'

    def execute(self, context):
        tree = context.space_data.edit_tree
        node = tree.nodes.get(self.node)

        connected_socket = node.outputs[0].connected_socket
        if connected_socket and connected_socket.bl_rna.name == 'BBN_enum_socket':
            items = [x.value for x in connected_socket.enum_items]

            node.enum_items.clear()
            for x in items:
                item = node.enum_items.add()
                item.value = x

            node.outputs[0].set_items(items)

        return {'FINISHED'}


last_enum_items = []


def get_enum_items(self, context):
    global last_enum_items
    last_enum_items = [(x.value, x.value, x.value, '', index) for index, x in enumerate(self.enum_items)]
    return last_enum_items


class BBN_string_enum(bpy.types.PropertyGroup):
    value: bpy.props.StringProperty()


class BBN_node_enum(Node, BBN_node):
    bl_idname = 'bbn_enum_node'
    bl_label = "Const Enum"
    bl_icon = 'IPO_CONSTANT'

    enum_items: bpy.props.CollectionProperty(type=BBN_string_enum)
    value: bpy.props.EnumProperty(items=get_enum_items, update=BBN_tree.value_updated)

    output_sockets = {
        'Value': {'type': 'BBN_enum_socket'},
    }

    dependent_classes = [BBN_string_enum, BBN_OP_setup_enum_items]

    def draw_label(self):
        return f'Enum: {self.value}'

    def draw_buttons(self, context, layout):
        row = layout.row(align=True)
        row.prop(self, "value")
        row.operator('bbn.setup_enum_items', text='', icon='FILE_REFRESH').node = self.name

    def process(self, context, id, path):
        self.outputs['Value'].set_value(self.value)
